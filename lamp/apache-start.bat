
@echo off

if [%1]==[] goto usage

SET LOCATION=%CD%
echo %LOCATION%

SET WWW=%1
echo %WWW%

SET DEFAULT_SITE=%LOCATION%/apache2/sites-available/000-default.conf
SET PHP_INI=%LOCATION%/php5/apache2/php.ini

docker rm -f apache-php
docker run -v %DEFAULT_SITE%:/etc/apache2/sites-available/000-default.conf -v %PHP_INI%:/etc/php5/apache2/php.ini -v %WWW%:/var/www/html --name apache -p 80:80 -p 443:443 -d --link mysql:mysql eboraas/apache-php
docker exec apache a2enmod proxy
docker exec apache a2enmod rewrite
docker exec apache apachectl restart
docker exec apache apt-get update
docker exec apache-php apt-get install -y mariadb-client
docker exec apache apt-get install -y pv

goto :eof

:usage
@echo Usage: docker-start.bat [public http folder]
exit /B1