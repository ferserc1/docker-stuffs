
#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: docker-start.sh [public http directory path]"
    exit
fi

WWW=$1

LOCATION="$(pwd)"

DEFAULT_SITE=${LOCATION}/apache2/sites-available/000-default.conf
PHP_INI=${LOCATION}/php5/apache2/php.ini

docker run -v ${WWW}:/home --name mysql -e MYSQL_ROOT_PASSWORD=develpass -d -p 3306:3306 mysql:5.7
./apache-start.sh $1
