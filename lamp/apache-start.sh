
#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Usage: apache-start.sh [public http directory path]"
    exit
fi

WWW=$1

LOCATION="$(pwd)"

DEFAULT_SITE=${LOCATION}/apache2/sites-available/000-default.conf
PHP_INI=${LOCATION}/php5/apache2/php.ini

docker rm -f apache-php
docker run -v ${DEFAULT_SITE}:/etc/apache2/sites-available/000-default.conf -v ${PHP_INI}:/etc/php5/apache2/php.ini -v ${WWW}:/var/www/html --name apache-php --net=host -d eboraas/apache-php
#docker run -v ${DEFAULT_SITE}:/etc/apache2/sites-available/000-default.conf -v ${PHP_INI}:/etc/php5/apache2/php.ini -v ${WWW}:/var/www/html --name apache-php -p 80:80 -p 443:443 -d --link mysql:mysql eboraas/apache-php 
docker exec apache-php a2enmod proxy
docker exec apache-php a2enmod rewrite
docker exec apache-php apachectl restart
docker exec apache-php apt-get update
docker exec apache-php apt-get install -y mariadb-client --force-yes
docker exec apache-php apt-get install -y pv --force-yes
