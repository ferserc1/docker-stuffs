
@echo off

if [%1]==[] goto usage

SET LOCATION=%CD%
echo %LOCATION%

SET WWW=%1
echo %WWW%

SET DEFAULT_SITE=%LOCATION%/apache2/sites-available/000-default.conf

docker run -v %WWW%:/home --name mysql -e MYSQL_ROOT_PASSWORD=develpass -d -p 3306:3306 mysql:5.7
apache-start.bat %WWW%

goto :eof

:usage
@echo Usage: docker-start.bat [public http folder]
exit /B1