
@echo off

if [%1]==[] goto createContainer

@echo Sorry, you only can share mongodb database folder in a Linux machine.

goto :eof

:createContainer
docker run --name mongodb -d -p 127.0.0.1:27017:27017 mongo


