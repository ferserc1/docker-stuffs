#!/bin/bash

if [ "$#" -ne 1 ]; then
	docker run --name mongodb -d -p 127.0.0.1:27017:27017 mongo:3.4
else
	echo "Mapping mongodb database to $1"
	docker run --name mongodb -d -p 127.0.0.1:27017:27017 -v "${1}":/data/db mongo:3.4
fi
